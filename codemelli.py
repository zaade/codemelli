#!/usr/bin/env python3

"""
A simple calculator for generating and validating Iranian national code numbers.

Iranian national numbers are a 10-digit number.
From left to right, it can be broken down to:
    - digit 1-3: a code assigned to the county where the person's birth has been recorded
    - digit 4-5: a code assigned to the year of birth
    - digit 6-9: a sequential number assigned to a specific person
    - digit 10: a checksum to make sure that the rest of the number has been entered correctly

See https://fa.wikipedia.org/wiki/%DA%A9%D8%A7%D8%B1%D8%AA_%D9%87%D9%88%D8%B4%D9%85%D9%86%D8%AF_%D9%85%D9%84%DB%8C


TODO: use https://vakiltop.com/blog/national-code/
or https://calc.worldi.ir/iranian-national-identity-card/
to also print informational data about a national code.

"""

import argparse
import random


def checksum(national_number: str) -> int:
    assert len(national_number) == 9, "wrong parameter length"
    assert national_number.isdigit(), "wrong parameter value"
    summation: int = 0
    for i, digit in enumerate(national_number):
        summation += int(digit) * (10 - i)
    remainder = summation % 11
    csum = ""
    if remainder < 2:
        csum = str(remainder)
    else:
        csum = str(11 - remainder)
    return csum


def main():
    parser = argparse.ArgumentParser(description="calculate national code")
    parser.add_argument("code", type=str, help="national code")
    parser.add_argument(
        "--verify", action="store_true", default=False, help="verify checksum"
    )
    parser.add_argument(
        "--checksum",
        action="store_true",
        default=False,
        help="calculate and print checksum",
    )
    parser.add_argument(
        "--random",
        action="store_true",
        default=False,
        help="produce random national number",
    )

    args = parser.parse_args()

    if args.verify:
        csum = checksum(str(args.code)[:9])
        if csum == args.code[9]:
            print("code is valid")
        else:
            print("code is not valid")
        return

    if args.checksum:
        csum = checksum(str(args.code)[:9])
        print(f"{args.code[:9]}{csum}")
        return

    if args.random:
        code = "".join([str(random.randint(0, 9)) for _ in range(9)])
        csum = checksum(code)
        print(f"{code}{csum}")
        return


if __name__ == "__main__":
    main()
